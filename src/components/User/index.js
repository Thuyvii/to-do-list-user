import React, {useState} from "react";
import '../../style/todoItem.css';
import axios from 'axios';
import Popup from "reactjs-popup";
import  Modal from "react-modal";



function Todo({ todo, remove, update }) {
  const [isEditing, setIsEditing] = useState(false);
  const [name, setUser] = useState(todo.name);
  const [modalIsOpen, setModalIsOpen] = useState(false)

  const deleteUser = evt => {
    axios.delete(`https://60190360971d850017a4082e.mockapi.io/users/${evt.target.id}`);
    //console.log(evt.target.id);
    remove(evt.target.id);
  };
  const editUser = () => {
    setIsEditing(!isEditing);
  };
  const userUpdate = evt => {
    evt.preventDefault();
    axios.put(`https://60190360971d850017a4082e.mockapi.io/users/${todo.id}`, {name: `${name}`});
    update(todo.id, name);
    editUser();
  };
  const handleChange = evt => {
    setUser(evt.target.value);
  };

  

  let result;
  if (isEditing) {
    result = (
      <div className="Todo">
        <form className="Todo-edit-form">
          <input onChange={handleChange} value={name} type="text" />
          <button onClick={userUpdate}>Save</button>
        </form>
      </div>
    );
  } else {
    result = (
      <div className="Todo">
        <li
          id={todo.id}
          className="Todo-task male"
          onClick={() => setModalIsOpen(true)}
        >
          {todo.name}
        </li>

        <Modal isOpen={modalIsOpen} shouldCloseOnOverlayClick={false} onRequestClose={() => setModalIsOpen} className="modal-content"> 
          <div class="modal-header">
            <span className="close" onClick={() => setModalIsOpen(false)}>&times;</span>
            <h2>Detail User</h2>
          </div>
          <div class="modal-body">
            <table>
              <tr>
                <td>Name:</td>
                <td
                id={todo.id}
                className="Todo-task"
                onClick={() => setModalIsOpen(true)}
              >
                {todo.name}
              </td>
              </tr>
              <tr>
                <td>Birthday: </td>
                <td
                id={todo.id}
                className="Todo-task"
                onClick={() => setModalIsOpen(true)}
              >
                {todo.birthday}
              </td>
              </tr>
              <tr>
                <td>Gender: </td>
                <td
                id={todo.id}
                className="Todo-task"
                onClick={() => setModalIsOpen(true)}
              >
                {todo.gender}
              </td>
              </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button onClick={() => setModalIsOpen(false)} className="btn-info">Close</button>
          </div>


        </Modal>

        <div className="Todo-buttons">
          <button onClick={editUser} data-toggle="modal" target="#edit">
            <i className="fas fa-pen" />
          </button>

          <button onClick={deleteUser}>
            <i id={todo.id} className="fas fa-trash" />
          </button>
        </div>
        
      </div>
    );
  }
  return result;
}

export default Todo;

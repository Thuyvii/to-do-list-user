import React, { Component } from "react";
import styled from 'styled-components'

function Detail(isOpen, shouldCloseOnOverlayClick, onRequestClose) {
  const [modalIsOpen, setModalIsOpen] = useState(false)
  return(
      <div>
        <Modal isOpen={modalIsOpen} shouldCloseOnOverlayClick={false} onRequestClose={() => setModalIsOpen}>
          <h1>Hello</h1>
          <button onClick={() => setModalIsOpen(false)}>Close</button>
        </Modal>
      </div>
  );
}

export default Detail;
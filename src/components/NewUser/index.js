import React, {useState} from 'react';
import Create from './Create'

function NewUser() {
  const [todos, setTodos] = useState([]);
  const create = newTodo => {
    console.log(newTodo);
    setTodos([...todos, newTodo]);
  };
  return (
    <div>
      <Create createTodo={create}/>
    </div>
  );
}
export default NewUser;
import React, { useReducer, useState } from "react";
import {v4 as uuid} from "uuid";
import "../../style/newTodo.css";
import '../../style/App.css';
import axios from 'axios';
import { useForm} from "react-hook-form";
import _ from "lodash/fp";


function NewTodo({createTodo }) {
  const {register, errors, reset} = useForm();

  const [userInput, setUserInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {
      name: "", birthday: "", gender:""
    }
  );

  const handleChange = evt => {
    setUserInput({ [evt.target.name]: evt.target.value });
  };


  const handleSubmit = evt => {
    evt.preventDefault();
    if (userInput.name !== "")
    {
      console.log(userInput.gender);
      const newUser = { id: uuid(), name: userInput.name, birthday: userInput.birthday, gender: userInput.gender};
      axios.post(`https://60190360971d850017a4082e.mockapi.io/users`, newUser);
      createTodo(newUser);
      setUserInput({ name: "" , birthday: "", gender:""});
      reset();
    } else {
        console.log("Plese enter new User...");
    }
  };

  return (
    <form className="form-group">
    <input
      className="form-control"
      value={userInput.name}
      onChange={handleChange}
      id="name"
      type="text"
      name="name"
      placeholder="Enter name User"
      ref={register({ required: true, minLength: 2 })}
    />
    {_.get("name.type", errors) === "required" && (
        <p>This field is required</p>
      )}
    <input
      className="form-control"
      value={userInput.birthday}
      onChange={handleChange}
      id="birthday"
      type="date"
      name="birthday"
      placeholder="Enter birthday"
    />
      <label className="form-check-label"><input
      className="form-check-input" 
      onChange={handleChange}
      type="radio"
      name="gender"
      value="Nam"
    />Nam</label>
    <label className="form-check-label"><input
      className="form-check-input"
      onChange={handleChange}
      type="radio"
      name="gender"
      value="Nữ"
    />Nữ</label>
    <label className="form-check-label"><input
      className="form-check-input"
      onChange={handleChange}
      type="radio"
      name="gender"
      value="Khác"
    />Khác</label>
    <button style={{display: 'block'}} onClick={handleSubmit}>Add</button>
  </form>
  );
}

export default NewTodo;

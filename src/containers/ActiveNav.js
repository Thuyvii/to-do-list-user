import React from "react";
import {
  Link,
  useRouteMatch
} from "react-router-dom";

function ActiveNav({ label, to, activeOnlyWhenExact }) {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact
  });
  console.log(match);

  return (
    <div className={match ? "btn-info" : ""}>
      
      <Link to={to}>{label}</Link>
    </div>
  );
}
export default ActiveNav;
import '../style/App.css';
import React, {useState, useEffect} from 'react';
import Todo from '../components/User/index';
import NewTodo from '../components/NewUser';
import axios from 'axios';

function TodoList() {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const fecthData = async() => {
    const result = await axios(
      'https://60190360971d850017a4082e.mockapi.io/users',
    );
    setTodos(result.data);
    }
    fecthData();
  }, []);

  const buttons = ([
    { name: "all", label: "All" },
    { name: "complete", label: "Completed" }
  ]);


  const remove = id => {
    setTodos(todos.filter(todo => todo.id !== id));
  };

  const update = (id, updatedName) => {
    const updatedTodos = todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, name: updatedName };
      }
      return todo;
    });
    setTodos(updatedTodos);
  };



  const  todoList = todos.map(todo => (
    <Todo
      update={update}
      remove={remove}
      key={todo.id}
      todo={todo}
    />
  ));

  const onFilterChange = name => {
    let arr = todos;
    if (name === "completed") {
      arr = arr.filter(task => task.completed);
    } else {
      arr = arr.filter(task => !task.completed);
    }
    console.log(arr);
  };

  const filter = (items, filter) => {
    switch (filter) {
      case "all":
        return items;
      case "completed":
        return items.filter(item => item.completed);
      default:
        return items;
    }
  }

  const listFilter =  buttons.map(({name, label}) => {
    const isActive = filter === name;
    const clazz = isActive ? "btn-info" : "btn-primary";
    return (
      <button
        type="button"
        className={`btn ${clazz}`}
        key={name}
        onClick={() => onFilterChange(name)}
      >
        {label}
      </button>
    );
});

    return (
      <div className="TodoList">
        <h1>List Users</h1>
        <ul>{todoList}</ul>
      </div>
    );
  }

export default TodoList;

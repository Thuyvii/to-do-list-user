import React, {useState} from 'react';
import NewUser from './components/NewUser/index';
import Home from './containers/UserList';
import NotFound from './containers/NotFound';
import OldLink from './containers/ActiveNav'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {
  return (
    <Router>
    <div>
      <nav className="navbar navbar-inverse">
        <ul className="nav navbar-nav">
          <li>
          <div className="navbar-brand">Logo</div>
          </li>
          <li className="navbar-brand">
            <OldLink activeOnlyWhenExact={true} to="/" label="Home"/>
          </li>
          <li className="navbar-brand">
            <OldLink to="/new" label="New" />
          </li>
        </ul>
      </nav>
      <hr />
      <Switch>
          <Route path="/new">
            <NewUser/>
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route component={NotFound} />
        </Switch>
    </div>
    </Router>
  );
}

export default App;